# About Function

An Amazon SES notification handler for processing bounces, complaints and deliveries to upload to an Amazon S3 bucket.

# Requirement

- An Amazon S3 Bucket
- An Amazon SNS Topic configured with Amazon SES Notifications

# Usage

1. Create SNS Topic
2. Create SES Identity Management with a setting to send all notifications to SNS Topic
3. Create S3 bucket with Expiration Lifecycle rule
4. Create this Lambda Function with the Bucket name
5. Subscribe the lambda endpoint to the SNS Topic
6. Allow s3:PutObject action to created IAM role for the bucket. Example of policy:

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:PutObject"
            ],
            "Resource": [
                "arn:aws:s3:::{BUCKET}/*"
            ]
        }
    ]
}

```

# License

MIT License (MIT)

This software is released under the MIT License, see license.txt.
