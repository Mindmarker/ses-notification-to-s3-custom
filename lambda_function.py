from __future__ import print_function
import boto3
import json
import os
import time;
from dateutil import parser


bucket = os.environ['BUCKET']
s3 = boto3.client('s3')

def lambda_handler(event, context):
    message = json.loads(event['Records'][0]['Sns']['Message'])
    notification_type = message['notificationType']
    timestamp, report = handlers.get(notification_type, handle_unknown_type)(message)
    keyname = get_keyname(message, timestamp)
    save_notification(keyname, report)


def get_keyname(message, timestamp):
    notification_type = message['notificationType']
    headers = get_headers(message)
    filename = "EmailId: %s. Email: %s. ProcessTime: %s" % \
        (get_header(headers, 'X-Email-Id'), get_header(headers, 'X-Email-Receiver'), time.time())
    keyname = get_header(headers, 'X-Email-Server-Name') + '/' + parser.parse(timestamp).strftime('%Y-%m-%d') + '/' + notification_type + '/' + filename
    return keyname


def save_notification(keyname, report):
    print('Saving s3://' + bucket + '/' + keyname + ':' + report)
    s3.put_object(Bucket=bucket, Key=keyname , Body=report)


def handle_bounce(message):
    message_id = message['mail']['messageId']
    bounced_recipients = message['bounce']['bouncedRecipients']
    addresses = list(
        recipient['emailAddress'] for recipient in bounced_recipients
    )
    bounce_type = message['bounce']['bounceType']
    timestamp = message['bounce']['timestamp']
    report = "Message bounced when sending to %s. Bounce type: %s" % \
        (", ".join(addresses), bounce_type)
    return (timestamp, report)


def handle_complaint(message):
    message_id = message['mail']['messageId']
    complained_recipients = message['complaint']['complainedRecipients']
    addresses = list(
        recipient['emailAddress'] for recipient in complained_recipients
    )
    timestamp = message['complaint']['timestamp']
    report = "A complaint was reported by %s." % \
          (", ".join(addresses))
    return (timestamp, report)


def handle_delivery(message):
    message_id = message['mail']['messageId']
    delivery_timestamp = message['delivery']['timestamp']
    timestamp = message['delivery']['timestamp']
    report = "Message was delivered to %s successfully at %s" % \
          (", ".join(message['delivery']['recipients']), delivery_timestamp)
    return (timestamp, report)


def get_headers(message):
    headers = {}
    for item in message['mail']['headers']:
        headers[item['name']] = item['value']
    return headers


def get_header(headers, name):
    if name in headers:
        return headers[name]
    print("Unknown header name:%s" % \
        name)
    raise Exception("Invalid header name received: %s" % \
                    name)


def handle_unknown_type(message):
    print("Unknown message type:\n%s" % json.dumps(message))
    raise Exception("Invalid message type received: %s" % \
                    message['notificationType'])


handlers = {"Bounce": handle_bounce,
            "Complaint": handle_complaint,
            "Delivery": handle_delivery}